import Vue from 'vue'

import BootstrapVue from 'bootstrap-vue'

import Router from 'vue-router'

import VueAxios from 'vue-axios'
import axios from 'axios'

import Dashboard from '@/components/Dashboard.vue'
import Orders from '@/components/Orders.vue'

Vue.use(BootstrapVue)
Vue.use(Router)
Vue.use(VueAxios, axios)

export default new Router({
  routes: [
    { name: 'Dashboard', path: '/', component: Dashboard },
    { name: 'Orders', path: '/recent/orders/', component: Orders }
  ]
})
